#!/usr/bin/perl

$name="Ellie";
print "Hello, $name.\n";	# $name and \n evaluated
# The single quote implies a 	# Single quote implies everything is literal
print 'Hello, $name.\n';	# String is literal; newline not
	# interpreted

print "I don't care!\n";	# \n is interpreted in double quotes
print 'I don\'t care!', "\n";	# Backslash protects single quote 
	# in string �don\�t�
print "\n\n";
print 'This is a single quote \n \n $name.';
print "\n"
