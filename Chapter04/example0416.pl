#!/usr/bin/perl
# Program: stricts.test
# Script to demonstrate the strict pragma
# Use function allows you to use modules located in the standard library
# Use Strict looks for bare words, if any are found the program is aborted
# use strict "subs";
$name = Ellie;             # Unquoted word Ellie
print "Hi $name.\n";
